using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tic_Tac.Models
{
    public class Model1
    {
        [Range(1, 1000, ErrorMessage= "enter within range")]
        public int TicId { get; set; }
        public int TicList { get; set; }
    }
}